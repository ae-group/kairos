""" models for announcements between members """
from django.conf import settings                            # type: ignore
from django.db import models                                # type: ignore
from filer.fields.image import FilerImageField              # type: ignore

from kairos.utils import member_full_name


class AnnounceCategory(models.Model):
    """ member announcement categories """
    ac_name = models.CharField(max_length=39)
    ac_image = FilerImageField(blank=True, null=True, on_delete=models.SET_NULL)

    class Meta:         # pylint: disable=missing-class-docstring,too-few-public-methods
        verbose_name_plural = "AnnounceCategories"
        constraints = (models.UniqueConstraint(fields=['ac_name'], name='unique_category_name'), )

    def __str__(self):
        return f"{self.__class__.__name__}/{self.ac_name}"


class MemberAnnouncement(models.Model):
    """ member announcements offered or requested/searched by members """
    ma_user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    ma_announce_category = models.ForeignKey(AnnounceCategory, on_delete=models.CASCADE)
    ma_language = models.CharField(max_length=3, choices=settings.LANGUAGES)
    ma_action = models.CharField(max_length=12)     # e.g. offer/request/rent
    ma_description = models.TextField()
    ma_last_updated = models.DateTimeField(auto_now=True)    # needed for to determine the latest entries/changes

    class Meta:         # pylint: disable=missing-class-docstring,too-few-public-methods
        constraints = (models.UniqueConstraint(fields=['ma_user', 'ma_announce_category', 'ma_language', 'ma_action'],
                                               name='unique_user_category_language_action'), )

    def __str__(self):
        # used as record title in django admin views/lists
        return (f"{self.__class__.__name__}"
                f"@{member_full_name(self.ma_user)}"
                f"={self.ma_language}"
                f"-{self.ma_last_updated}"
                f"/{self.ma_announce_category.ac_name}"
                f"_{self.ma_action}"
                f":{self.ma_description}"
                )
