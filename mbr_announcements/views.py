""" announcements views. """
import datetime

from typing import Any, Optional
from urllib.parse import urlparse

from django.contrib.auth.models import User                             # type: ignore
from django.db.models import Q                                          # type: ignore
from django.http import HttpRequest, HttpResponseRedirect               # type: ignore
from django.urls import reverse                                         # type: ignore
from django.utils import timezone                                       # type: ignore
from django.utils.text import slugify                                   # type: ignore
from django.utils.translation import gettext as _, override             # type: ignore
from django.views.generic import ListView                               # type: ignore

from ae.django_utils import set_url_part, requested_language            # type: ignore

from kairos.middleware import HIDDEN_LANG_SESSION_KEY
from kairos.utils import send_chg_notif
from .models import AnnounceCategory, MemberAnnouncement


EXC_CAT_ACT_SEP = '+c+a+'
EXC_ADD_MARKER = 'EmptyMemberAnnouncement'
EXC_ANCHOR_ID_PREFIX = "MAnnAIdP"


def check_change_notification(log_desc: str, rec: MemberAnnouncement, current_user: User, request: HttpRequest,
                              chk_desc: str = "", url: str = "", notify_members: Optional[bool] = True):
    """ check changes on MemberAnnouncement record and send notifications if yes.

    :param log_desc:            announcement description text to be logged/send-in-notification, which is on update the
                                new one and on deletion the actual one.
    :param rec:                 added/updated/deleted announcement record data (author, category and action).
    :param current_user:        record of current user.
    :param request:             http request, needed for error notification.
    :param chk_desc:            old announcement description text on update, unspecified or "" on deletion.
    :param url:                 absolute url to show the announcement (unspecified or "" on deletion).
    :param notify_members:      pass False to not send notifications to all members (only send to MCN_LOGGERS).
    """
    if log_desc != chk_desc or not notify_members:
        with override(rec.ma_language):
            item = _("{ma_cat_name}-{ma_action}-announcement").format(ma_cat_name=rec.ma_announce_category.ac_name,
                                                                      ma_action=rec.ma_action)
            mode = 'deleted' if chk_desc == "" else 'added' if EXC_ADD_MARKER in chk_desc else 'updated'
            send_chg_notif(mode, [log_desc], current_user, request,
                           owner=rec.ma_user, item=item, url=url, notify_members=notify_members)


def extend_context(context: dict[str, Any], request: HttpRequest, **kwargs):
    """ extend context for view and plugin (bundled here to avoid redundancies) """
    context['EXC_ADD_MARKER'] = EXC_ADD_MARKER
    context['EXC_CAT_ACT_SEP'] = EXC_CAT_ACT_SEP
    context['EXC_ANCHOR_ID_PREFIX'] = EXC_ANCHOR_ID_PREFIX
    context['url_kwargs'] = kwargs

    context['user_cat_acts'] = user_cat_acts = []  # in order to prevent duplicate member announcements per user
    current_user = request.user
    if current_user.is_authenticated:
        for ma_object in MemberAnnouncement.objects.filter(ma_user=current_user):
            user_cat_acts.append(ma_object.ma_announce_category.ac_name + EXC_CAT_ACT_SEP + ma_object.ma_action)


def get_queryset(request: HttpRequest):
    """ get full/filtered queryset of announcements/objects for view and plugin, bundled here to avoid redundancies. """
    current_user = request.user
    obj_list = MemberAnnouncement.objects

    user_filter = {}
    search_txt = request.GET.get('q')
    if search_txt:
        if search_txt.isnumeric():  # entered member id?
            user_filter['ma_user__username'] = search_txt
        else:
            user_filter['ma_description__contains'] = search_txt
    if request.GET.get('l') == '1':
        user_filter['ma_language'] = requested_language()
    if request.GET.get('u') == '1':
        user_filter['ma_user'] = current_user
    if user_filter:
        if current_user.is_staff:   # admins are seeing all the empty records added/owned by other users
            obj_list = obj_list.filter(Q(**user_filter) | Q(ma_description__contains=EXC_ADD_MARKER))
        else:
            obj_list = obj_list.filter(**user_filter)

    if current_user.is_anonymous:   # not current_user.is_authenticated (prevent 'AnonymousUser' object is not iterable)
        obj_list = obj_list.exclude(ma_description__contains=EXC_ADD_MARKER)
    elif not current_user.is_staff:  # only admins are seeing records added/owned by other users
        obj_list = obj_list.exclude(Q(ma_description__contains=EXC_ADD_MARKER) & ~Q(ma_user=current_user))

    # hide announcements of members that either were not logged in within the last year or were never logged in
    last_year_datetime = timezone.now() - datetime.timedelta(days=365)
    obj_list = obj_list.exclude(Q(ma_user__last_login=None) | Q(ma_user__last_login__lt=last_year_datetime))

    if hidden_languages := request.session.get(HIDDEN_LANG_SESSION_KEY, []):
        obj_list = obj_list.exclude(ma_language__in=hidden_languages)

    return obj_list.order_by("ma_announce_category__ac_name", "ma_action", "-ma_last_updated")


class AnnouncementsListView(ListView):
    """ collapsible list of member announcement actions, plus inline editing, deleting and adding them. """
    model = MemberAnnouncement

    def get_context_data(self, **kwargs):
        """ context data """
        context = super().get_context_data(**kwargs)
        # noinspection PyTypeChecker
        extend_context(context, self.request, **self.kwargs)
        return context

    def get_queryset(self):
        """ filter by currently selected language and order by category-name, action and newest changes first. """
        return get_queryset(self.request)

    def post(self, request: HttpRequest, *_args, **kwargs):
        """ handle member announcements add, delete and change of description. """
        current_user = request.user
        ma_id: Optional[int] = kwargs.get('id')
        if not ma_id:
            cat_name, ma_action, ma_user = kwargs['cat'], kwargs['act'], current_user
            row = MemberAnnouncement.objects.create(
                ma_user=ma_user,
                ma_announce_category=AnnounceCategory.objects.get(ac_name=cat_name),
                ma_language=MemberAnnouncement.objects.filter(ma_action=ma_action).first().ma_language,
                ma_action=ma_action,
                ma_description=f"{ma_user}@{datetime.datetime.now()}+{cat_name}/{ma_action}={EXC_ADD_MARKER}",
            )
            # pylint: enable=no-member # false positive .objects
            ma_id = row.pk
            url = reverse('announcement-edit', kwargs={'cat': slugify(cat_name), 'id': ma_id})

        else:
            cat_slug = kwargs['cat']
            notify_members = request.POST.get(f'notify_members_{ma_id}') == 'on'
            if request.path.split("/")[-3] == 'delete':
                del_rec = MemberAnnouncement.objects.get(id=ma_id)
                pre_rec = self.get_queryset().filter(ma_last_updated__lt=del_rec.ma_last_updated).first()
                if pre_rec:
                    ma_id = pre_rec.pk
                    url = reverse('announcement-show', kwargs={'cat': cat_slug, 'id': ma_id})
                else:
                    ma_id = 0
                    url = reverse('announcements-list')
                if EXC_ADD_MARKER not in (desc := del_rec.ma_description) and desc.strip():
                    check_change_notification(desc, del_rec, current_user, request, notify_members=notify_members)
                del_rec.delete()

            else:  # update
                url = reverse('announcement-show', kwargs={'cat': cat_slug, 'id': ma_id})
                upd_rec = MemberAnnouncement.objects.get(id=ma_id)
                old_desc = upd_rec.ma_description       # old rec desc gets changed by .update(ma_description=...)
                new_desc = request.POST['ma_description'].strip()
                if new_desc:
                    upd_rec.ma_description = new_desc
                    upd_rec.save()
                    check_change_notification(
                        new_desc, upd_rec, current_user, request,
                        chk_desc=old_desc,
                        url=set_url_part(request.build_absolute_uri(url), EXC_ANCHOR_ID_PREFIX + str(ma_id)),
                        notify_members=notify_members)

        url = set_url_part(url, EXC_ANCHOR_ID_PREFIX + str(ma_id), urlparse(request.META.get('HTTP_REFERER')).query)

        return HttpResponseRedirect(url)
