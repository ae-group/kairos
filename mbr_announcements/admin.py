""" register announcement categories model to be maintained with django admin. """
from django.contrib import admin                                    # type: ignore
from .models import AnnounceCategory, MemberAnnouncement

admin.site.register(AnnounceCategory)
admin.site.register(MemberAnnouncement)
