import os
import sys

from ae.base import load_dotenvs            # type: ignore


path = os.path.expanduser('~/kairos')       # results in '/home/AndiEcker/kairos' for my personal pythonanywhere account
if path not in sys.path:
    sys.path.append(path)

load_dotenvs()      # on pythonanywhere.com os.getcwd() is ~ folder (not project root), therefore put .env into ~ folder

os.environ['DJANGO_SETTINGS_MODULE'] = 'kairos.settings'

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
