""" member messages app models """
from django.conf import settings                            # type: ignore
from django.db import models                                # type: ignore

from kairos.utils import member_full_name


class MemberMessage(models.Model):
    """ member messages model """
    mm_author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    mm_language = models.CharField(max_length=3, choices=settings.LANGUAGES)
    mm_text = models.TextField()
    mm_public = models.BooleanField()
    mm_expired = models.DateField(null=True, blank=True)
    mm_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        # used as record title in django admin views/lists
        return (f"{self.__class__.__name__}"
                f"@{member_full_name(self.mm_author)}"
                f"={self.mm_language}"
                f"-{self.mm_created}"
                + (f"->{self.mm_expired}" if self.mm_expired else "")
                + ("/public" if self.mm_public else "")
                + f":{self.mm_text}"
                )
