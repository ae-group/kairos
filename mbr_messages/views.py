""" member messages views """
import datetime
import re

from typing import Any, Optional
from urllib.parse import urlparse

from django.contrib.auth.models import User                     # type: ignore
from django.db.models import Q                                  # type: ignore
from django.http import HttpRequest, HttpResponseRedirect       # type: ignore
from django.urls import reverse                                 # type: ignore
from django.utils.translation import gettext as _               # type: ignore
from django.views.generic import ListView                       # type: ignore

from ae.django_utils import set_url_part                        # type: ignore

from kairos.middleware import HIDDEN_LANG_SESSION_KEY
from kairos.utils import send_chg_notif
from mbr_messages.models import MemberMessage


MSG_ADD_MARKER = "EmptyMemberMessage"
MSG_ANCHOR_ID_PREFIX = "MMsgAIdP"

MOBILE_AGENT_RE = re.compile(r".*(iphone|mobile|androidtouch)", re.IGNORECASE)


def check_change_notification(old_public: bool, old_expired: Optional[datetime.date], old_lang: str, old_text: str,
                              new_public: bool, new_expired: Optional[datetime.date], new_lang: str, new_text: str,
                              current_user: User, author: User, url: str, notify_members: bool, request: HttpRequest):
    """ check for changes and if yes then send notifications to receivers.

    :param old_public:          old publication status of message.
    :param old_expired:         old message expiry date or None.
    :param old_lang:            old message language.
    :param old_text:            old message text.
    :param new_public:          new publication status.
    :param new_expired:         new expire date or None.
    :param new_lang:            new message language.
    :param new_text:            new message text.
    :param current_user:        user record of currently authenticated user.
    :param author:              user record of message author.
    :param url:                 absolute url to show the changed message on the website.
    :param notify_members:      pass False to not send notifications to all members (only send to MCN_LOGGERS).
    :param request:             http request, needed for error notification.
    """
    action = 'added' if MSG_ADD_MARKER in old_text else 'updated'
    changes = []
    if action == 'added':
        item = _("public message") if new_public else _("private message")
        if new_expired:
            item += " " + _("expiring {new_expired}").format(new_expired=new_expired)
        changes.append(new_text)
    else:
        item = _("message")
        prefix = "- "

        if old_public != new_public:
            changes.append(prefix + (_("made public") if new_public else _("revoked publication")))

        if old_expired != new_expired:
            if not old_expired:
                changes.append(prefix + _("added expiry date {new_expired}").format(new_expired=new_expired))
            elif not new_expired:
                changes.append(prefix + _("removed expiry date {old_expired}").format(old_expired=old_expired))
            else:
                changes.append(prefix + _("changed expiry date from {old_expired} to {new_expired}").format(
                    old_expired=old_expired, new_expired=new_expired))

        if old_lang != new_lang:
            changes.append(prefix + _("changed language from {old_lang} to {new_lang}").format(
                old_lang=old_lang, new_lang=new_lang))

        if old_text != new_text:
            changes.append(new_text)

    if changes or not notify_members:
        send_chg_notif(action, changes, current_user, request,
                       owner=author, item=item, url=url, notify_members=notify_members)


def extend_context(context: dict[str, Any], request: HttpRequest, **kwargs):
    """ extend context for view and plugin (to avoid redundancies) """
    context['MSG_ADD_MARKER'] = MSG_ADD_MARKER
    context['MSG_ANCHOR_ID_PREFIX'] = MSG_ANCHOR_ID_PREFIX
    context['is_mobile'] = MOBILE_AGENT_RE.match(request.META['HTTP_USER_AGENT'])
    context['url_kwargs'] = kwargs


def get_queryset(request: HttpRequest):
    """ get queryset of all objects for view and plugin (to avoid redundancies). """
    obj_list = MemberMessage.objects

    current_user = request.user
    if current_user.is_anonymous:   # not current_user.is_authenticated (prevent 'AnonymousUser' object is not iterable)
        obj_list = obj_list.exclude(mm_text__contains=MSG_ADD_MARKER).filter(mm_public=True)
    elif not current_user.is_staff:  # only admins are seeing records added/owned by other users
        obj_list = obj_list.exclude(Q(mm_text__contains=MSG_ADD_MARKER) & ~Q(mm_author=current_user))

    if hidden_languages := request.session.get(HIDDEN_LANG_SESSION_KEY, []):
        obj_list = obj_list.exclude(mm_language__in=hidden_languages)

    return obj_list.order_by("-mm_created")


class MemberMessageListView(ListView):
    """ member messages list view """
    model = MemberMessage

    def get_context_data(self, **kwargs):
        """ context data """
        context = super().get_context_data(**kwargs)
        extend_context(context, self.request, **self.kwargs)
        return context

    def get_queryset(self):
        """ filter by currently selected language and order by category-name, action and newest changes first. """
        return get_queryset(self.request)

    def post(self, request: HttpRequest, *_args, **kwargs):
        """ handle member message edit. """
        current_user = request.user
        mm_id: Optional[int] = kwargs.get('id')
        if not mm_id:   # action == 'add'
            row = MemberMessage.objects.create(
                mm_author=current_user,
                mm_language=request.LANGUAGE_CODE,
                mm_public=False,
                mm_text=f"{current_user}@{datetime.datetime.now()}={MSG_ADD_MARKER}",
            )
            mm_id = row.pk
            url = reverse('message-edit', kwargs={'id': mm_id})

        else:
            notify_members = request.POST.get(f'notify_members_{mm_id}') == 'on'
            if request.path.split("/")[-2] == 'delete':
                rec = MemberMessage.objects.get(id=mm_id)
                pre_rec = self.get_queryset().filter(mm_created__lt=rec.mm_created).first()
                if pre_rec:
                    mm_id = pre_rec.pk
                    url = reverse('message-show', kwargs={'id': mm_id})
                else:
                    mm_id = 0
                    url = reverse('messages-list')
                if MSG_ADD_MARKER not in (txt := rec.mm_text) and txt.strip():
                    send_chg_notif('deleted', ([f"- {rec.mm_expired}"] if rec.mm_expired else []) + [txt],
                                   current_user, request,
                                   owner=rec.mm_author,
                                   item=_("public message") if rec.mm_public else _("private message"),
                                   notify_members=notify_members)
                rec.delete()

            else:   # == 'update'
                url = reverse('message-show', kwargs={'id': mm_id})
                rec = MemberMessage.objects.get(id=mm_id)
                old_public, old_expired, old_lang, old_text = (
                    rec.mm_public, rec.mm_expired, rec.mm_language, rec.mm_text)
                new_public, new_expired, new_lang, new_text = (
                    request.POST.get('mm_public') == 'on',      # if checked: 'on', None/not-in-POST-dict if unchecked
                    request.POST['mm_expired'] and datetime.date.fromisoformat(request.POST['mm_expired']) or None,
                    request.POST['mm_language'],
                    request.POST['mm_text'].strip())

                if new_text:    # prevent to wipe the text
                    rec.mm_public, rec.mm_expired, rec.mm_language, rec.mm_text = (
                        new_public, new_expired, new_lang, new_text)
                    rec.save()
                    check_change_notification(
                        old_public, old_expired, old_lang, old_text,
                        new_public, new_expired, new_lang, new_text,
                        current_user, rec.mm_author,
                        set_url_part(request.build_absolute_uri(url), MSG_ANCHOR_ID_PREFIX + str(mm_id)),
                        notify_members, request)

        url = set_url_part(url, MSG_ANCHOR_ID_PREFIX + str(mm_id), urlparse(request.META.get('HTTP_REFERER')).query)

        return HttpResponseRedirect(url)
