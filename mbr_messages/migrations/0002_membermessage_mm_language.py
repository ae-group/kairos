# Generated by Django 3.1.14 on 2024-06-11 13:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mbr_messages', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='membermessage',
            name='mm_language',
            field=models.CharField(choices=[('en', 'English'), ('es', 'Español'), ('de', 'Deutsch')], default='de', max_length=3),
            preserve_default=False,
        ),
    ]
