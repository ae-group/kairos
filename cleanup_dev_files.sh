rm .gitignore
rm .gitlab-ci.yml
rm ./*.md
rm ./*.rst
rm ./*.txt
rm setup.py
rm -r .git
rm -r docs
rm -r tests
