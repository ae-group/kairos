import os
import sys

from ae.base import load_dotenvs                        # type: ignore

path = '/home/kairosgomera/kairos'
if path not in sys.path:
    sys.path.append(path)

load_dotenvs()

os.environ['DJANGO_SETTINGS_MODULE'] = 'kairos.settings'

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
