""" member meetings model """
from django.conf import settings                                # type: ignore
from django.db import models                                    # type: ignore

from kairos.utils import member_full_name


class MemberMeeting(models.Model):
    """ member meetings model """
    mt_author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    mt_language = models.CharField(max_length=3, choices=settings.LANGUAGES)
    mt_text = models.TextField()
    mt_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        # used as record title in django admin views/lists
        return (f"{self.__class__.__name__}"
                f"@{member_full_name(self.mt_author)}"
                f"={self.mt_language}"
                f"-{self.mt_created}"
                f":{self.mt_text}"
                )
