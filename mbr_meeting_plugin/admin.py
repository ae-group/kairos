""" register member meetings model to be maintainable within django admin. """
from django.contrib import admin                            # type: ignore
from .models import MemberMeeting

admin.site.register(MemberMeeting)
