""" member meetings plugin """
from django.conf import settings                                    # type: ignore

from cms.models import CMSPlugin                                    # type: ignore
from cms.plugin_base import CMSPluginBase                           # type: ignore
from cms.plugin_pool import plugin_pool                             # type: ignore

from ae.django_utils import requested_language                      # type: ignore

from kairos.middleware import HIDDEN_LANG_SESSION_KEY
from mbr_meeting_plugin.models import MemberMeeting
from mbr_meeting_plugin.views import MBR_MEET_ANCHOR_ID_PREFIX


@plugin_pool.register_plugin
class MemberMeetingsPlugin(CMSPluginBase):
    """ member meetings plug-in """
    model = CMSPlugin
    render_template = "membermeetings_plugin.html"
    cache = False

    def render(self, context, instance, placeholder):
        """ add anchor/lang prefix and meeting texts in all available languages onto context. """
        context = super().render(context, instance, placeholder)

        context['MBR_MEET_ANCHOR_ID_PREFIX'] = MBR_MEET_ANCHOR_ID_PREFIX
        context['mt_texts'] = mtt = {}
        hidden_languages = context['request'].session.get(HIDDEN_LANG_SESSION_KEY, [])
        for lang, _lang_name in settings.LANGUAGES:
            if lang not in hidden_languages:
                mts = MemberMeeting.objects.filter(mt_language=lang).order_by('-mt_created').first()
                mtt[lang] = mts.mt_text if mts else ""
        context['mt_lang_text'] = mtt[requested_language()]

        return context
