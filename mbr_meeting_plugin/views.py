""" view to add a new member meeting text. """
from django.conf import settings                                    # type: ignore
from django.http import HttpRequest                                 # type: ignore
from django.shortcuts import redirect                               # type: ignore
from django.utils.translation import override                       # type: ignore
from django.views import View                                       # type: ignore

from ae.django_utils import requested_language                      # type: ignore

from mbr_meeting_plugin.models import MemberMeeting
from kairos.utils import send_chg_notif


MBR_MEET_ANCHOR_ID_PREFIX = 'MMeetAIdP'


class MemberMeetingAddView(View):
    """ add new member meeting with text block, language, author, created date. """
    @staticmethod
    def post(request: HttpRequest, *_args, **_kwargs):
        """ form post to add new member meeting text into the database. """
        current_user = request.user
        updated_languages = []
        for lang, _lang_name in settings.LANGUAGES:
            updated_text = request.POST.get(lang + '_text')
            if updated_text != request.POST.get(lang + '_old_text'):
                MemberMeeting.objects.create(
                    mt_author=current_user,
                    mt_language=lang,
                    mt_text=updated_text,
                )
                updated_languages.append(lang)

            with override(lang):
                send_chg_notif('meeting', [updated_text], current_user, request,
                               notify_members=request.POST.get(lang + '_notify') == 'on')

        return redirect(request.META.get('HTTP_REFERER', '/') + '#' + MBR_MEET_ANCHOR_ID_PREFIX
                        + (updated_languages and updated_languages[0] or requested_language()))
