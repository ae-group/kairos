features and use-cases
**********************

this open source website project can be freely used and adapted to any exchange or sharing association or platform.

on the resulting website, association members can announce their exchange offers and requests, as well as post internal
or public messages.

not only the members and their data, but also all the pages structure and content of this website are freely designable
and maintainable by association members with special administration permissions. all permissions are configurable
individually for each member.

ids, names and phone numbers of members are only visible for authenticated members.
visitors of this website can see only the announcements texts and explicitly published messages.

depending on your role you find more information in the :ref:`member manual`, the :ref:`administrator manual` or the
:ref:`programmer manual` of this website project.
