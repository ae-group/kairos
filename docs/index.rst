############################################
kairos django project website documentation
############################################

welcome to the documentation of the django website project **kairos**.


.. include:: features_and_examples.rst


.. toctree::

    member_manual
    administrator_manual
    programmer_manual


indices and tables
******************

* :ref:`genindex`
* :ref:`modindex`
