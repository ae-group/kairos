""" helpers to show member name and to process notifications on change of member messages/announcements. """
import os

from typing import Iterable, Optional, Union

from django.contrib import messages                             # type: ignore
from django.contrib.auth.models import User                     # type: ignore
from django.db import models                                    # type: ignore
from django.http import HttpRequest                             # type: ignore
from django.utils.translation import gettext as _               # type: ignore

from ae.notify import Notifications                             # type: ignore


def member_full_name(member_rec: Union[User, models.ForeignKey]) -> str:
    """ return the full and unique display-name of a member.

    :param member_rec:          member user record.
    :return:                    unique member name in the format username-first_name.
    """
    return _("{member_id}-{name}").format(member_id=member_rec.username, name=member_rec.first_name)


# get optional MCN_* (MemberChangesNotifications) service config env vars, loaded by settings.py if not in exported
LOGGER_RECEIVERS = os.environ.get("MCN_LOGGERS", "mailto:aecker2@gmail.com=96-Andi").split(',')
MEMBER_RECEIVERS = os.environ.get("MCN_RECEIVERS", "mailto:aecker2@gmail.com=96-Andi").split(',')
notification_service = Notifications(
    smtp_server_uri=os.environ.get("MCN_MAIL_URI", ""), mail_from=os.environ.get("MCN_MAIL_FROM", "website@kairos.org"),
    telegram_token=os.environ.get("MCN_TG_TOK", ""),
    whatsapp_token=os.environ.get("MCN_WA_TOK", ""), whatsapp_sender=os.environ.get("MCN_WA_FROM", ""),
)


def send_chg_notif(mode: str, changes: Iterable[str], current_user: User, request: HttpRequest,
                   owner: Optional[User] = None, item: str = "", url: str = "", subject: Optional[str] = None,
                   notify_members: Optional[bool] = True):
    """ send out change notifications of meeting/data-changes to log (MCN_LOGGERS) and all members (MCN_RECEIVERS).

    :param mode:                data item change type: 'added', 'updated' or 'deleted' or 'meeting' for member meetings.
    :param changes:             list/tuple of changes done to the data item / meeting text.
    :param current_user:        user record of the authenticated user (current user or admin).
    :param request:             http request, needed for error notification.
    :param owner:               data item owner user record, current user or None (specifying current user as modifier).
    :param item:                name/description of the changed item (ignored if mode == 'meeting').
    :param url:                 absolute url to show data item (ignored/not-used if mode in ('deleted', 'meeting')).
    :param subject:             overwrite default notification subject text.
    :param notify_members:      pass False to NOT send notifications to all members (only send to MCN_LOGGERS).
    """
    if subject is None:
        subject = _("notification from kairos website")

    if mode == 'added':
        msg = "<a href=\"{url}\">{item}</a> of {owner} got added"
    elif mode == 'updated':
        msg = "<a href=\"{url}\">{item}</a> of {owner} got changed"
    elif mode == 'deleted':
        msg = "<i>{item}</i> of {owner} got deleted"
    elif mode == 'meeting':
        msg = "<i>Member Meeting</i> notification"
    else:
        msg = "<a href=\"{url}\">{item}</a> of {owner} got {mode}"

    username = current_user.username
    msg = _(msg).format(owner=member_full_name(owner) if owner else username, item=item, url=url, mode=mode)

    if not owner or username != owner.username:
        msg += " (" + _("by {admin}").format(admin=member_full_name(current_user)) + ")"

    for change in changes:
        msg += f"<br><br>{change}"

    for change_receiver in LOGGER_RECEIVERS + (MEMBER_RECEIVERS if notify_members else []):
        err_msg = notification_service.send_notification(msg, change_receiver, subject)
        if err_msg:
            print(f"***   send_chg_notif() error with message '{msg}' to '{change_receiver}': {err_msg}")
            messages.error(request, err_msg)
