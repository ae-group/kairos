""" project urls registration """
from cms.sitemaps import CMSSitemap                                         # type: ignore
from django.conf import settings                                            # type: ignore
from django.conf.urls.i18n import i18n_patterns                             # type: ignore
from django.conf.urls.static import static                                  # type: ignore
from django.contrib import admin                                            # type: ignore
from django.contrib.auth.views import LogoutView                            # type: ignore
from django.contrib.sitemaps.views import sitemap                           # type: ignore
from django.contrib.staticfiles.storage import staticfiles_storage          # type: ignore
from django.contrib.staticfiles.urls import staticfiles_urlpatterns         # type: ignore
from django.urls import include, path                                       # type: ignore
from django.views.generic import RedirectView                               # type: ignore

from mbr_announcements.views import AnnouncementsListView
from mbr_meeting_plugin.views import MemberMeetingAddView
from mbr_messages.views import MemberMessageListView
from kairos.views import (
    MemberEmailSignupView, MembersListView, MemberLoginView,
    member_lang_hide, member_lang_show, signup_email_confirm, signup_paid_confirm)


admin.autodiscover()

favicon_url = staticfiles_storage.url("favicon.ico")

urlpatterns = [
    path('sitemap.xml', sitemap, {"sitemaps": {"cmspages": CMSSitemap}}),
    path('favicon.ico', RedirectView.as_view(url=favicon_url)),
]
urlpatterns += i18n_patterns(
    path('accounts/login/', MemberLoginView.as_view(), name='login'),               # overwrite login in auth.urls.py
    path('accounts/logout/', LogoutView.as_view(next_page="/"), name='logout'),     # overwrite logout in auth.urls.py
    # not used because gets redirected to the previous page/referer in kairos/templates/registration/login.html
    path('accounts/profile/', RedirectView.as_view(url="/")),                       # redirect to home after login
    path('accounts/', include('django.contrib.auth.urls')),

    path('signup/member/', MemberEmailSignupView.as_view(), name='signup-member'),  # CMS-page reverse name mbr-signup
    path('signup/confirm/<str:key>/<str:token>/', signup_email_confirm, name='signup-confirm'),
    path('signup/paid/<str:keys>/<str:token>/', signup_paid_confirm, name='signup-paid'),

    path("admin/", admin.site.urls),

    # all url kwargs are slugified with exception to 'announcement-add' which is using capitalized category/action names
    path('announcements/list/', AnnouncementsListView.as_view(), name='announcements-list'),
    path('announcements/add/<str:cat>/<str:act>', AnnouncementsListView.as_view(), name='announcement-add'),
    path('announcements/edit/<str:cat>/<int:id>', AnnouncementsListView.as_view(), name='announcement-edit'),
    path('announcements/update/<str:cat>/<int:id>', AnnouncementsListView.as_view(), name='announcement-update'),
    path('announcements/delete/<str:cat>/<int:id>', AnnouncementsListView.as_view(), name='announcement-delete'),
    path('announcements/show/<str:cat>/<int:id>', AnnouncementsListView.as_view(), name='announcement-show'),

    path('meetings/update/', MemberMeetingAddView.as_view(), name='meetings-update'),

    path('messages/list/', MemberMessageListView.as_view(), name='messages-list'),
    path('messages/add/', MemberMessageListView.as_view(), name='message-add'),
    path('messages/edit/<int:id>', MemberMessageListView.as_view(), name='message-edit'),
    path('messages/update/<int:id>', MemberMessageListView.as_view(), name='message-update'),
    path('messages/delete/<int:id>', MemberMessageListView.as_view(), name='message-delete'),
    path('messages/show/<int:id>', MemberMessageListView.as_view(), name='message-show'),

    path('members/list/', MembersListView.as_view(), name='members-list'),

    path('member_language_hide/<str:lang>/', member_lang_hide, name='member-lang-hide'),
    path('member_language_show/<str:lang>/', member_lang_show, name='member-lang-show'),

    path('', include('cms.urls')),
)

if settings.DEBUG:  # only needed when using runserver
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
else:
    urlpatterns += staticfiles_urlpatterns()
