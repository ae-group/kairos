""" kairos middleware classes """
from django.conf import settings                                    # type: ignore


# added session dict keys for extra/hideable member languages (redundant/hardcoded in kairos/templates/base_navbar.html)
HAS_HIDEABLE_LANG_SESSION_KEY = 'page_has_hideable_mbr_languages'   #: True if cms page show multilang member content
HIDDEN_LANG_SESSION_KEY = 'hidden_mbr_languages'                    #: list with language ids to hide

HIDEABLE_LANG_REVERSE_ID_SUFFIX = '_hl'     #: cms page ids with this marker can contain member content in extra lang


class MemberLanguages:
    """ extend request session dict with keys/variables to control and hide member languages and their filtering. """
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if page := request.current_page:
            session = request.session
            session[HAS_HIDEABLE_LANG_SESSION_KEY] = hhl = HIDEABLE_LANG_REVERSE_ID_SUFFIX in page.reverse_id

            hidden_languages = session.get(HIDDEN_LANG_SESSION_KEY, [])
            if hhl:
                req_language = request.LANGUAGE_CODE
                if request.GET.get('l') == '1':     # user clicked on current-language-only button in announcements page
                    hidden_languages = [_[0] for _ in settings.LANGUAGES if _[0] != req_language]
                elif request.GET.get('l') == '0':   # user clicked on all-languages/EU button in announcements page
                    hidden_languages = []
                elif req_language in hidden_languages:
                    hidden_languages = [_ for _ in hidden_languages if _ != req_language]
            # no need to set `session.modified = True` because we are using a new list instance
            session[HIDDEN_LANG_SESSION_KEY] = hidden_languages

        response = self.get_response(request)

        return response
