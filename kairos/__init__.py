"""
website for barter rings to communicate, exchange and share goods and time
==========================================================================

TODO:
-----

- (PyCharm bug) register show_menu from menus lib to fix "Unresolved tag" inspection warning in PyCharm IDE:

    * tried adding 'builtins': ["menus.templatetags.menu_tags"] to settings.TEMPLATES[0]['OPTIONS']

    * tried adding the following code to kairos.__init__.py/.settings.py/.asgi.py/.wsgi.py/.url.py/...
        # register show_menu from menus lib to fix "Unresolved tag" inspection warning in PyCharm IDE
        from django import template
        from menus.templatetags.menu_tags import ShowMenu
        register = template.Library()
        register.tag('show_menu', ShowMenu)

    * tried adding 'menus.templatetags', to settings.INSTALLED_APPS

    * one workaround was to add the tag name 'show_menu' into the register.tag(ShowMenu) call as 1st argument
      into menus.template_tags.menu_tags.py line 173

- (PyCharm bug) fix wrong inspection "Failed to resolve url..." in url templatetags, e.g. {% url 'login' %} in
   kairos/templates/registration/login.html

- (PyCharm bug) "Closing tag..." inspection if forloop.last is used (e.g. memberannouncement_list.html)

- (PyCharm bug) {% include %} shows warning "Unresolved template reference..." although in same folder
  (memberannouncement)

- (PyCharm bug) inspection PyUnresolvedReferences on self.ma_user.username in mbr_announcements/models.py

- fix cms_toolbar/navbar overlap - the following workarounds don't work:

    .. code-block:: css
        :force:

        // prevent overlap of cms_toolbar and navbar from https://stackoverflow.com/questions/32027903
        .cms-toolbar-expanded body, .cms-toolbar-expanded .navbar-fixed-top {
            top: 30px;
        }
        // prevent cms_toolbar/navbar overlap https://github.com/django-cms/django-cms/issues/6242 & 6392
        // .. cms-toolbar-expanded class get added as class to html element when the cms toolbar is visible
        html.cms-toolbar-expanded {
            .navbar-fixed-top, .fixed-top {
                top: 46px;
            }
            .modal-dialog {
                margin-top: 3.5rem;
            }
        }

    .. code-block:: javascript

        // not tried using javascript:
        if(document.getElementById("html").classList.contains("cms-toolbar-expanded")){
            // cms toolbar is visible, so but now I set/change the "*top" css using js
        }

- make spacing of content nicer to not get overlapped by navbar (currently using 4 div row elements in base.html)

"""

__version__ = '0.3.107'
