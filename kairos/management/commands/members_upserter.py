""" command line tool to insert or update rows of a database table.

the data format of the member data is coming as ods (LibreOffice spreadsheet) and has to be converted with the following
command into a csv file::

    libreoffice --headless --convert-to csv "Liste Kontaktdaten 2022-10-10.ods"

after the conversion make sure that the csv is only containing lines with data and that the first line is specifying the
username (member id) and the field/column names to be updated. make sure no line seperator is at the last data line.
example csv file content::

    username,first_name,last_name
    1,Adam,Apfel <linesep>
    ... <linesep>
    999,Xaver,Zappa

to run this script without specifying the CSV filename/path, make sure that the environment variable
``MEMBERS_DATA_FILE_PATH`` is set (e.g. via the .env file) to the csv file name and path that contains the member data
to be created or updated.

big thanks to Adam Johnson for his blog on how to implement dry-run for Django manage commands (see
https://adamj.eu/tech/2022/10/13/dry-run-mode-for-data-imports-in-django/).
"""
import argparse
import csv
import os

from contextlib import closing
from io import TextIOWrapper
from typing import Any, Callable, TextIO, Union

import django                                                           # type: ignore
from django.core.management.base import BaseCommand, no_translations    # type: ignore
from django.db.transaction import atomic                                # type: ignore
from django.contrib.auth import get_user_model                          # type: ignore
from django.contrib.auth.hashers import make_password                   # type: ignore

from ae.base import load_dotenvs                                        # type: ignore

# prepare django apps/env in order to import create_page_user() and the models from mbr_announcements and default
# .. value of the command argument for the CSV filename to import (env var MEMBERS_DATA_FILE_PATH).
load_dotenvs()          # get import file paths from environment vars in .env file if not declared in local OS shell
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'kairos.settings')
django.setup()

# late import because django.setup() has to be executed before
# pylint: disable-next=ungrouped-imports,wrong-import-position
from django.contrib.auth.models import Group         # type: ignore # noqa: E402
from cms.api import create_page_user                 # type: ignore # pylint: disable=wrong-import-position # noqa: E402


FieldNames = tuple[str, ...]
Member = tuple[str, ...]
Members = list[Member]


class Command(BaseCommand):
    """ manage.py command to update or insert members with their name and phone numbers. """
    help = "Update or insert member names and phone numbers from a CSV file"

    def add_arguments(self, parser: argparse.ArgumentParser) -> None:
        """ define manage command arguments. """
        parser.add_argument("file", nargs='?', type=argparse.FileType(), default=os.environ['MEMBERS_DATA_FILE_PATH'],
                            help="filename (and path) of the CSV file to import")

        parser.add_argument("-f", "--force", action="store_true", default=False,
                            help="ignore errors and continue import if one of the members can not be updated/inserted")

        parser.add_argument("-r", "--reset", action="store_true", default=False,
                            help="reset/recreate member entries (even if they already exists in the database)")

    @no_translations
    def handle(self, file: TextIOWrapper, *args: Any, **options: Any) -> None:  # pylint: disable=arguments-differ
        """ execute the members_upsert manage.py command. """
        force = options.pop('force')
        reset = options.pop('reset')
        verbosity = options.pop('verbosity', 1)

        print_fn = self.stdout.write
        if verbosity >= 2:
            if args:
                print_fn(f"####  ignoring additional command arguments ({args})")
            if force:
                print_fn("###   --force command option specified")
            if reset:
                print_fn("###   --reset command option specified")
        if verbosity >= 3 and options:
            print_fn(f"##    ignoring additional command keyword arguments ({options})")

        with closing(file), atomic():
            col_names, member_data = load_members(file, print_fn)
            if col_names:
                upsert_members(col_names, member_data, force, reset, verbosity, print_fn)


def _clean_member_name(name: str) -> str:
    """ remove second names and dots from the passed name. """
    first_name, *_ = name.split(" ")
    return first_name.replace(".", "")


def _ensure_password(field_names: FieldNames, member: Member) -> tuple[FieldNames, Member, bool]:
    """ convert/add password for new created member entry if not specified and the member's first name is specified. """
    new_default = False
    if 'password' in field_names:
        idx = field_names.index('password')
        member = *member[:idx], make_password(member[idx]), *member[idx + 1:]
    elif 'first_name' in field_names:
        field_names += ('password', )
        member += (make_password((f"{member[field_names.index('first_name')]}{member[0]}" * 3)[:8]), )
        new_default = True
    return field_names, member, new_default


def load_members(file_handle: Union[TextIOWrapper, TextIO], print_fn: Callable) -> tuple[FieldNames, Members]:
    """ load member data from CSV file (first row specifying the field/column-names, first column is username/memberId).

    :param file_handle:         file handle of CSV file to import.
    :param print_fn:            print function to output to console.
    :return:                    tuple with field names tuple and a list of tuples with the loaded member data.
    """
    try:
        reader = csv.reader(file_handle)
        field_names = tuple(next(reader))
        if len(field_names) <= 1:
            print_fn(f"***   import file {file_handle.name} has no column header in 1st line nor column data to upsert;"
                     f" expected header with column-/field-names, where first is 'username' and 2nd to nth contains"
                     f" one of the field names of the Members table, like e.g. 'first_name', 'last_name', 'email', ...")
            return (), []

        if field_names[0] != 'username':
            print_fn(f"***   import file {file_handle.name} format error: header is missing 1st field name 'username'")
            return (), []

    except (StopIteration, OSError, csv.Error, Exception) as ex:    # pylint: disable=broad-except
        print_fn(f"***   import file {file_handle.name} is empty/corrupt - nothing to upsert (internal error: {ex})")
        return (), []

    members = []
    try:
        for row in reader:
            cols = []
            for idx, col in enumerate(row):
                if field_names[idx] == 'first_name':
                    col = _clean_member_name(col)
                cols.append(col.strip())
            members.append(tuple(cols))

    except (StopIteration, OSError, csv.Error, Exception) as ex:    # pylint: disable=broad-except
        print_fn(f"****  loading member from CSV file {file_handle.name} at line {reader.line_num} raised error: {ex}")

    return field_names, members


def upsert_members(fields: FieldNames, members: Members, reset: bool, force: bool, verbosity: int, print_fn: Callable):
    """ upsert member rows in the database.

    :param fields:              tuple of field/column names. first field name is the username/pk to identify the member.
    :param members:             list of tuples with field/column data.
    :param force:               force upsert (pass True to ignore/skip exception/err on processing of a single member).
    :param reset:               pass True to reset (delete and re-insert) an already existing member entry.
    :param verbosity:           verbosity level (0...3) of django manage.py command.
    :param print_fn:            print function for user output on console/stdout.
    :return:
    """
    usr_model = get_user_model()
    admin_user = usr_model.objects.get(username='admin', id=1)
    members_group = Group.objects.get(name='members')  # get already existing 'members' group

    for member in members:
        usr_nam = member[0]
        ext_names, member, new_pwd = _ensure_password(fields, member)
        defaults = {field: member[1 + idx] for idx, field in enumerate(ext_names[1:-1 if new_pwd else len(ext_names)])}
        if verbosity >= 1:
            print_fn(f"----  data of user {usr_nam} loaded as: {defaults}")

        if reset:
            try:
                usr_model.objects.get(username=usr_nam).delete()    # fully reset/recreate member user record if exists
            except (usr_model.DoesNotExist, AttributeError, ValueError, Exception) as ex:  # pylint:disable=broad-except
                if verbosity >= 2:
                    print_fn(f" ---  username {usr_nam} does not exist - creating new entry, ignoring exception {ex}")

        try:
            user_row, created = usr_model.objects.update_or_create(defaults=defaults, username=usr_nam)
        except (usr_model.DoesNotExist, AttributeError, ValueError, Exception) as ex:
            print_fn(f"***  error {ex} on upsert of username {usr_nam}")
            if force:
                continue
            raise ex
        if verbosity >= 1:
            print_fn(f"----  {'created' if created else 'updated'} user {usr_nam} in user model table")

        if created:
            try:
                if new_pwd:
                    user_row.password = member[-1]
                    user_row.save()
                    if verbosity >= 2:
                        print_fn(" ---  added default password for new/recreated member entry")
                page_user = create_page_user(
                    admin_user, user_row,
                    can_add_page=False, can_view_page=False, can_change_page=False, can_delete_page=False,
                    can_recover_page=False,
                    can_add_pageuser=False, can_change_pageuser=False, can_delete_pageuser=False,
                    can_add_pagepermission=False, can_change_pagepermission=False, can_delete_pagepermission=False)
                page_user.is_staff = False
                page_user.save()
                if verbosity >= 2:
                    print_fn(" ---   added page user record")
                user_row.groups.add(members_group)
                if verbosity >= 2:
                    print_fn(" ---   added user to members group")
            except (AttributeError, ValueError, Exception) as ex:   # pylint: disable=broad-except
                print_fn(f"***  error {ex} on classifying newly created user {usr_nam} as no-staff member")
                if not force:
                    raise ex
