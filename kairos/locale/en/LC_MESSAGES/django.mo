��          �      �       0     1     N     [     j     p     �  B   �  !   �  E         J     k  "   �  3  �  X   �  �   ;  	   �     �     �  P   �  �   7  M   �  z        �  ^   �  K   �                         	                 
                (%(ma_act)s in %(cat_name)s) CurrLanguage Greetings from Hello KairosBrandName add %(ma_act)s in %(cat_name)s check to show this message publicly to any visitor of this website delete %(ma_act)s in %(cat_name)s enter the date until this message will be visible in the website news notification from kairos website save %(ma_act)s in %(cat_name)s text of %(ma_act)s in %(cat_name)s Project-Id-Version: kairos
Report-Msgid-Bugs-To: https://gitlab.com/ae-group/kairos/-/issues
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: Andi Ecker <aecker2@gmail.com>
Language-Team: English
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 (enter text for the new announcement action '%(ma_act)s' in the '%(cat_name)s' category) English
 click here to select other language
 haze click aquí para seleccionar otro idioma
 klicke hier um eine andere Sprache auszuwählen Love from Hi Kairos La Gomera add an announcement for the '%(ma_act)s' action into the category '%(cat_name)s' check to show this message publicly to any visitor of this website or leave this checkbox unchecked to display this message only to authenticated members delete this '%(ma_act)s' announcement action from the category '%(cat_name)s' enter the date until this message will be visible in the website news or leave this field entry to never hide this message   save the entered announcement text for this '%(ma_act)s' action in the category '%(cat_name)s' text of the announcement action '%(ma_act)s' in the category '%(cat_name)s' 