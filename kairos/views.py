""" project core views for user/member management. """
import os
from typing import Any, Optional

from django.conf import settings                                                # type: ignore
from django.contrib import messages                                             # type: ignore
from django.contrib.auth import get_user_model                                  # type: ignore
from django.contrib.auth.models import Group, User                              # type: ignore # noqa: E402
from django.contrib.auth.tokens import PasswordResetTokenGenerator              # type: ignore
from django.contrib.auth.views import LoginView                                 # type: ignore
from django.db.models import IntegerField, Q                                    # type: ignore
from django.db.models.functions import Cast                                     # type: ignore
from django.http import HttpRequest                                             # type: ignore
from django.shortcuts import render, redirect                                   # type: ignore
from django.template.loader import render_to_string                             # type: ignore
from django.urls import reverse                                                 # type: ignore
from django.utils.encoding import force_bytes, force_str                        # type: ignore
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode      # type: ignore
from django.utils.translation import gettext as _                               # type: ignore
from django.views.generic import ListView, View                                 # type: ignore

from kairos.forms import MemberEmailSignupForm, MemberLoginForm
from kairos.middleware import HIDDEN_LANG_SESSION_KEY
from kairos.utils import member_full_name, notification_service, send_chg_notif
from mbr_messages.models import MemberMessage
from mbr_messages.views import MSG_ANCHOR_ID_PREFIX


MAX_MEMBER_ID = 999
SIGNUP_ADMINS = os.environ.get(
    "SIGNUP_ADMINS", "mailto:aecker2@gmail.com=96-Andi,telegram:5294745680=96-Andi").split(',')


def _member_lang_toggle(request: HttpRequest, lang: str, show: bool):
    hidden_languages = request.session.get(HIDDEN_LANG_SESSION_KEY, [])
    last_url = request.META.get('HTTP_REFERER', '/')
    if show:
        hidden_languages = [l_ for l_ in hidden_languages if l_ != lang]
        last_url = last_url.replace("&l=1", "")     # last request.GET.get('l') == '1'
    else:
        hidden_languages.append(lang)
        if "&l=0" in last_url and len(hidden_languages) == len(settings.LANGUAGES) - 1:
            last_url = last_url.replace("&l=0", "&l=1")
    request.session[HIDDEN_LANG_SESSION_KEY] = hidden_languages

    return redirect(last_url)


def member_lang_hide(request: HttpRequest, lang: str):
    """ hide content written in the specified language """
    return _member_lang_toggle(request, lang, False)


def member_lang_show(request: HttpRequest, lang: str):
    """ show content written in the specified language """
    return _member_lang_toggle(request, lang, True)


class _MemberSignupTokenGenerator(PasswordResetTokenGenerator):
    """ token generator for member signup with email verification. """
    def _make_hash_value(self, user, timestamp):
        return str(user.pk) + str(user.is_active) + str(user.username) + str(timestamp)


_member_signup_token = _MemberSignupTokenGenerator()


def _duplicate_members(username: str, phone: str, email: str, exclude_pk: int = 0) -> list[User]:
    dup_filter = Q(pk__in=[])   # https://forum.djangoproject.com/t/improving-q-objects-with-true-false-and-none/851
    if username:
        dup_filter |= Q(username=username)
    if phone:
        dup_filter |= Q(last_name=phone)
    if email:
        dup_filter |= Q(email=email)

    return get_user_model().objects.exclude(Q(pk=exclude_pk) | Q(is_staff=True)).filter(dup_filter)


def _dup_member_messages(dup_members: list[User], request: HttpRequest,
                         msg_prefix: str, username: str, phone: str, email: str) -> bool:
    for dup_mbr in dup_members or []:
        messages.error(request, f"{msg_prefix}: " + _("Member data duplicate error(s) with {name}:").format(
            name=member_full_name(dup_mbr)))
        if username and dup_mbr.username == username:
            messages.warning(request, _("Member Id") + ": " + username)
        if phone and dup_mbr.last_name == phone:
            messages.warning(request, _("Phone") + ": " + phone)
        if email and dup_mbr.email == email:
            messages.warning(request, _("Email") + ": " + email)

    return bool(dup_members)


def _activate_member(member_pk: int, request: HttpRequest, token: Optional[str] = None) -> tuple[bool, User, str]:
    """ activate member account identified by member_pk, returning activation status, member record and msg prefix. """
    user_model = get_user_model()
    username = "?"
    try:
        member_rec = user_model.objects.get(pk=member_pk)

        username = str(-int(member_rec.username))   # == member_rec.username[1:]
        dup_members = _duplicate_members(username, member_rec.last_name, member_rec.email, member_pk)

    except (TypeError, ValueError, OverflowError, user_model.DoesNotExist, Exception):  # pylint: disable=broad-except
        member_rec = dup_members = None

    msg_prefix = _("Admission Fee Payment Confirmation")
    activated = False

    if not member_rec or dup_members is None:
        messages.error(request, f"{msg_prefix}: " + _("Invalid member ids {}!").format(str(member_pk) + "/" + username))
    elif token and not _member_signup_token.check_token(member_rec, token):
        messages.error(request, f"{msg_prefix}: " + _("Registration {} expired error!").format(token))
    elif member_rec.is_active or member_rec.username[0] != "-":    # or int(member.username) > 0:
        messages.warning(request, f"{msg_prefix}: " + _("Account of member {name} got already activated!").format(
            name=member_full_name(member_rec)))
    elif not _dup_member_messages(dup_members, request, msg_prefix, username, member_rec.last_name, member_rec.email):
        member_rec.is_active = True
        member_rec.username = username
        member_rec.groups.add(Group.objects.get(name='members'))
        member_rec.save()

        current_user = request.user
        rec = MemberMessage.objects.create(
            mm_author=current_user,
            mm_language=request.LANGUAGE_CODE,
            mm_text=_("A 'warm welcome' to our new member {name}").format(name=member_full_name(member_rec)),
            mm_public=False,
        )
        url = reverse('message-show', kwargs={'id': rec.pk}) + '#' + MSG_ANCHOR_ID_PREFIX + str(rec.pk)
        send_chg_notif('added', [rec.mm_text], current_user, request,
                       owner=current_user, item=_("private message"), url=request.build_absolute_uri(url))

        activated = True

    return activated, member_rec, msg_prefix


def _activate_and_confirm_member(member_pk: int, request: HttpRequest, token: Optional[str] = None):
    activated, member_rec, subject_prefix = _activate_member(member_pk, request, token)
    if activated:
        url = request.build_absolute_uri(reverse('login'))
        msg = render_to_string('registration/payment_received_email_body.html',
                               {'admin_name': member_full_name(request.user), 'member': member_rec, 'url': url})
        subject = _("Account activated")
        receiver_name = member_full_name(member_rec)
        notification_service.send_email(msg, member_rec.email, subject_prefix + " - " + subject, receiver_name)
        messages.success(request, subject + " (" + receiver_name + ")")

    return redirect(reverse('members-list') + ('#' + member_rec.username if activated else ""))


def signup_email_confirm(request: HttpRequest, key: str, token: str):
    """ view from link in signup confirmation email to check. """
    user_model = get_user_model()
    try:
        member_pk: Optional[int] = int(force_str(urlsafe_base64_decode(key)))
        member_rec: Optional[Any] = user_model.objects.get(pk=member_pk)
    except (TypeError, ValueError, OverflowError, user_model.DoesNotExist, Exception):  # pylint: disable=broad-except
        member_pk = member_rec = None

    subject = _("Email Address Verification")
    if member_rec is None:
        messages.error(request, subject + ": " + _("Invalid member ids {}!").format(key + "->" + str(member_pk)))
    elif not _member_signup_token.check_token(member_rec, token):
        messages.error(request, subject + ": " + _("Registration {} expired error!").format(token))
    else:
        mbr_url = request.build_absolute_uri(reverse('members-list') + '#' + member_rec.username)
        for admin_data in SIGNUP_ADMINS:
            admin_addr, admin_name = admin_data.split('=')
            url = request.build_absolute_uri(reverse('signup-paid', kwargs={
                'keys': urlsafe_base64_encode(force_bytes(admin_name + ',' + str(member_pk))),
                'token': _member_signup_token.make_token(member_rec),
            }))
            msg = render_to_string('registration/payment_request_email_body.html',
                                   {'admin_name': admin_name, 'member': member_rec, 'act_url': url, 'mbr_url': mbr_url})
            err_msg = notification_service.send_notification(msg, admin_data, _("Admission Fee Payment Confirmation"))
            if err_msg:
                messages.error(request, _("Message-Send-Error to '{admin}' on sign-up of '{name}': {err_msg}").format(
                    admin=f"{admin_name} ({admin_addr.split(':')[0]})", name=member_rec.first_name, err_msg=err_msg))

    return render(request, 'registration/email_verification.html', {'member': member_rec})


def signup_paid_confirm(request: HttpRequest, keys: str, token: str):
    """ confirmation of signup admin that the new signup member has paid. """
    admin_name, member_key = force_str(urlsafe_base64_decode(keys)).split(',')
    if not request.user.is_authenticated or member_full_name(request.user) != admin_name:
        msg = _("Hi {admin}, please first log in, then try again to confirm the receive of this admission fee payment."
                ).format(admin=admin_name)
        messages.error(request, msg)
        return redirect('login')

    return _activate_and_confirm_member(int(member_key), request, token=token)


class MemberEmailSignupView(View):
    """ handle member signup/registration. """
    form_class = MemberEmailSignupForm
    template_name = 'registration/signup_form.html'

    def get(self, request: HttpRequest, *_args, **_kwargs):
        """ GET request displaying empty form. """
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request: HttpRequest, *_args, **_kwargs):
        """ POST request from signup form submitted by new member. """
        form = self.form_class(request.POST)

        if not form.is_valid():
            messages.error(request, form.errors)
            return render(request, self.template_name, {'form': form})

        phone = form.cleaned_data.get('phone')
        email = form.cleaned_data.get('email')
        dup_members = _duplicate_members("", phone, email)
        if _dup_member_messages(dup_members, request, _("Member Sign-Up"), "", phone, email):
            return render(request, self.template_name, {'form': form})

        # create locked account and mark it as waiting to be verified/confirmed (negative username and is_active==False)
        usr_model = get_user_model()
        member_id = usr_model.objects.exclude(Q(is_staff=True) | Q(is_active=False)).count()
        member_rec = form.save(commit=False)
        member_rec.last_name = phone
        member_rec.is_active = False
        while True:
            member_id += 1
            if member_id > MAX_MEMBER_ID:
                messages.error(request, _("Invalid member ids {}!").format(str(member_id) + " > " + str(MAX_MEMBER_ID)))
                return render(request, self.template_name, {'form': form})
            if usr_model.objects.filter(username=str(-member_id)) or usr_model.objects.filter(username=str(member_id)):
                continue
            member_rec.username = str(-member_id)
            member_rec.save()
            break

        messages.info(request, _("Your sign-up got initiated and your member account created..."))

        email = member_rec.email      # == form.cleaned_data.get('email')
        url = request.build_absolute_uri(reverse('signup-confirm', kwargs={
            'key': urlsafe_base64_encode(force_bytes(str(member_rec.pk))),
            'token': _member_signup_token.make_token(member_rec),
        }))
        msg = render_to_string('registration/email_verification_email_body.html', {'member': member_rec, 'url': url})
        messages.info(request, _("Sending verification email to {email}...").format(email=email))
        subject = _("KairosBrandName") + " " + _("Member Sign-Up") + " - " + _("Email Address Verification")
        err_msg = notification_service.send_email(msg, email, subject, member_full_name(member_rec))
        if err_msg:
            messages.warning(request, _("Please get in contact with us, because of the following email send error:"))
            messages.error(request, err_msg)
        else:
            messages.success(request, _("Please click on the confirmation link in the email we just sent to you!"))

        return redirect("/")


class MembersListView(ListView):
    """ List of members. """
    model = get_user_model()

    def get_ordering(self):
        """ return the field or fields to use for ordering the queryset. """
        return self.request.GET.get('orderBy', 'member_int')

    def get_queryset(self):
        """ select members hiding admins. """
        self.queryset = self.model.objects.filter(is_staff=False).annotate(member_int=Cast('username', IntegerField()))
        return super().get_queryset()

    @staticmethod
    def post(request: HttpRequest, *_args, **_kwargs):
        """ activate member. """
        member_pk = int(request.POST.get('activate'))
        return _activate_and_confirm_member(member_pk, request)


class MemberLoginView(LoginView):                                   # pylint: disable=too-many-ancestors
    """ member login view. """
    form_class = MemberLoginForm
