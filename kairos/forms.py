""" kairos project core forms to signup new member. """
from django import forms                                                                    # type: ignore
from django.conf import settings                                                            # type: ignore
from django.contrib.auth import get_user_model                                              # type: ignore
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm, UsernameField   # type: ignore
from django.core.validators import RegexValidator                                           # type: ignore
# noinspection PyProtectedMember
from django.utils.regex_helper import _lazy_re_compile                                      # type: ignore
from django.utils.translation import gettext_lazy as lazy_                                  # type: ignore

from crispy_forms.bootstrap import FieldWithButtons, StrictButton                           # type: ignore
from crispy_forms.helper import FormHelper                                                  # type: ignore
from crispy_forms.layout import Div, Hidden, Layout, Submit                                 # type: ignore


first_name_conf = settings.FIELD_VALIDATOR_RE_MSG['first_name']
first_name_validators = [RegexValidator(_lazy_re_compile(first_name_conf[0]), lazy_(first_name_conf[1]), 'invalid')]
phone_conf = settings.FIELD_VALIDATOR_RE_MSG['phone']
phone_validators = [RegexValidator(_lazy_re_compile(phone_conf[0]), lazy_(phone_conf[1]), 'invalid')]


class MemberEmailSignupForm(UserCreationForm):
    """ form to initiate member signup/registration per email. """
    error_css_class = "text-danger"      # added to tr-element class (and not to embedded th/label or td/input elements)
    required_css_class = "mb-3 mt-1 p-1 border shadow ae-glow"  # add to the field row (luckily all fields are required)

    # set use_required_attribute=False for all the fields, although they are required, to show messages instead of
    # .. browser error tooltips (e.g. "Please fill out this field" in Firefox, which are not visible on mobile devices).
    use_required_attribute = False

    first_name = forms.CharField(validators=first_name_validators, label=lazy_("First name"))
    phone = forms.CharField(initial="+", validators=phone_validators, min_length=9, max_length=24, label=lazy_("Phone"))
    email = forms.EmailField(max_length=69)

    data_collection = forms.BooleanField(label=lazy_(
        "With my registration I accept the <a href=\"/data-collection/\">rules of this barter circle</a>."))
    privacy_policy = forms.BooleanField(label=lazy_(
        "I have read and taken note of <a href=\"/privacy-policy/\">the information requirements</a> according to Art."
        " 13 and 14 DSGVO."))

    class Meta:                                                         # pylint: disable=too-few-public-methods
        """ form model configurations """
        model = get_user_model()
        fields = ('first_name', 'phone', 'email', 'password1', 'password2', )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-3 ae-glow"
        self.helper.field_class = "col-lg-9"

        self.helper.add_input(Submit('submit', lazy_("Member Sign-Up"), css_class="btn btn-outline-warning w-100"))
        # self.helper.form_method = 'post' is the default


class MemberLoginForm(AuthenticationForm):
    """ crispy login form. """
    error_css_class = "text-danger"      # added to tr-element class (and not to embedded th/label or td/input elements)
    required_css_class = "mb-3 mt-1 p-3 border shadow ae-glow"  # add css to the field row (both fields are required)

    # set use_required_attribute=False for all the fields, although they are required, to show messages instead of
    # .. browser error tooltips (e.g. "Please fill out this field" in Firefox, which are not visible on mobile devices).
    use_required_attribute = False

    username = UsernameField(
        label=lazy_("Member Id"),
        widget=forms.TextInput(attrs={'autofocus': True})
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Div(Div('username',
                    css_class='col-12'),
                Div(FieldWithButtons('password',
                                     StrictButton('<i class="fa fa-eye"></i>', id='passwordVisButton', type='button')),
                    css_class='col-12'),
                css_class='row')
        )
        self.helper.add_input(Submit(
            'submit', lazy_("Log in"), css_class="btn btn-outline-warning mt-3 w-100",
            title=lazy_("authenticate with your member id (without leading zeroes)")))
        # noinspection PyUnboundLocalVariable
        if self.request and 'HTTP_REFERER' in (meta := self.request.META) and 'login' not in meta['HTTP_REFERER']:
            self.helper.add_input(Hidden('next', meta['HTTP_REFERER']))
