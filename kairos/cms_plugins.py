""" core plugins.

using the text_enabled plugins in ckeditor/TextPlugin only works in admin edit mode, but is not even calling render()
in the published page (and therefore not included in the published page) - same for mbr_news_plugin/cms_plugins.py
"""
from cms.plugin_base import CMSPluginBase                   # type: ignore
from cms.plugin_pool import plugin_pool                     # type: ignore

from kairos.forms import MemberEmailSignupForm


@plugin_pool.register_plugin
class MembersListLinkPlugin(CMSPluginBase):
    """ plug-in to multi-language link to the members-list url/page. """
    render_template = "members_list_link_plugin.html"
    cache = False
    text_enabled = True


@plugin_pool.register_plugin
class MemberSignupLinkPlugin(CMSPluginBase):
    """ plug-in to multi-language link to the member signup url/page. """
    render_template = "member_signup_link_plugin.html"
    cache = False
    text_enabled = True


@plugin_pool.register_plugin
class MemberSignupFormPlugin(CMSPluginBase):
    """ plug-in to multi-language member signup form - finally not used/fully-working. """
    render_template = "signup_form_plugin.html"
    cache = False

    def render(self, context, instance, placeholder):
        """ add form to context """
        context = super().render(context, instance, placeholder)
        context['form'] = MemberEmailSignupForm(data=context['request'].POST or None)
        return context
