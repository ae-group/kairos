""" page link box plugin """
from cms.plugin_base import CMSPluginBase                   # type: ignore
from cms.plugin_pool import plugin_pool                     # type: ignore

from .models import PageLinkBoxModel


@plugin_pool.register_plugin
class PageLinkBoxPlugin(CMSPluginBase):
    """ example taken from https://docs.django-cms.org/en/release-3.8.x/how_to/custom_plugins.html """
    model = PageLinkBoxModel
    render_template = "page_link_plugin.html"
    cache = False
    text_enabled = True
