""" model of page link plugin """
from django.db import models                                    # type: ignore
from cms.models.fields import PageField                         # type: ignore
from cms.models.pluginmodel import CMSPlugin                    # type: ignore
from filer.fields.image import FilerImageField                  # type: ignore


class PageLinkBoxModel(CMSPlugin):
    """ bootstrap container box with an image, a text and a link to a cms page. """
    page_link = PageField(on_delete=models.CASCADE)
    page_image = FilerImageField(blank=True, null=True, on_delete=models.SET_NULL)
