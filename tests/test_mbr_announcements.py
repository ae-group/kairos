""" unit tests for django app mbr_announcements. """
from mbr_announcements import __doc__ as doc_string
from mbr_announcements.admin import AnnounceCategory as AcModel, MemberAnnouncement as MaModel
from mbr_announcements.cms_plugins import MemberAnnouncementsPlugin
from mbr_announcements.models import AnnounceCategory, MemberAnnouncement


class TestInit:
    def test_doc_string(self):
        assert doc_string


class TestAdmin:
    def test_admin_declared(self):
        assert AcModel
        assert MaModel


class TestCmsPlugins:
    def test_cms_plugins_declared(self):
        assert MemberAnnouncementsPlugin


class TestModels:
    def test_models_declared(self):
        assert AnnounceCategory
        assert MemberAnnouncement
