""" kairos website tests """
import os

from ae.base import TESTS_FOLDER                # type: ignore

from kairos import __version__ as kairos_project_version, __doc__ as kairos_doc_string
from kairos.management.commands import members_upserter
from kairos.templatetags.kairos_tags import flag_icon, full_name

from kairos.asgi import application as asgi_app
from kairos.cms_menus import SelectionAndOrdering
from kairos.cms_plugins import MembersListLinkPlugin, MemberSignupFormPlugin, MemberSignupLinkPlugin
from kairos.forms import (
    first_name_conf, first_name_validators, phone_conf, phone_validators, MemberEmailSignupForm, MemberLoginForm)
from kairos.urls import favicon_url, urlpatterns
from kairos.utils import member_full_name, MEMBER_RECEIVERS, notification_service, send_chg_notif
from kairos.views import (
    SIGNUP_ADMINS, MemberEmailSignupView, MembersListView, signup_email_confirm, signup_paid_confirm)
from kairos.wsgi import application as wsgi_app


class TestInit:
    def test_version(self):
        """ test existence of package version. """
        assert isinstance(kairos_project_version, str)
        assert kairos_project_version.count(".") == 2

    def test_docstring(self):
        """ test existence of package docstring. """
        assert isinstance(kairos_doc_string, str)

    def test_tests_folder_exists(self):
        """ test existence of tests folder. """
        assert os.path.isdir(TESTS_FOLDER)


class TestCommands:
    def test_members_upserter_command_declared(self):
        assert members_upserter.Command

    def test_members_upserter_command_instance(self):
        assert members_upserter.Command()

    # COMMENTED OUT BECAUSE BeautifulSoup4 is no longer in requirements.txt
    # def test_members_announcements_data_loader_declared(self):
    #     assert _members_and_announcements_data_loader


class TestTemplateTags:
    def test_tags_declared(self):
        assert flag_icon
        assert full_name


class TestAsgi:
    def test_asgi_application(self):
        assert asgi_app


class TestCmsMenus:
    def test_cms_menus_declared(self):
        assert SelectionAndOrdering is None     # lazy registering


class TestCmsPlugins:
    def test_cms_plugins_declared(self):
        assert MemberSignupFormPlugin
        assert MemberSignupLinkPlugin
        assert MembersListLinkPlugin


class TestForms:
    def test_forms_declared(self):
        assert first_name_conf
        assert first_name_validators
        assert phone_conf
        assert phone_validators
        assert MemberEmailSignupForm
        assert MemberLoginForm


class TestUrls:
    def test_urls_declared(self):
        assert favicon_url
        assert urlpatterns


class TestUtils:
    def test_utils_declared(self):
        assert member_full_name
        assert MEMBER_RECEIVERS
        assert notification_service
        assert send_chg_notif


class TestViews:
    def test_views_declared(self):
        assert SIGNUP_ADMINS
        assert MemberEmailSignupView
        assert MembersListView
        assert signup_email_confirm
        assert signup_paid_confirm


class TestWsgi:
    def test_wsgi_application(self):
        assert wsgi_app
